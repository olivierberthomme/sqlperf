Author: Olivier BERTHOMME

Date: 24.04.2017

Subject: Repository home changed

Hello,

I've changed the "master" repository to Git-Hub to get public reputation and because I never used private repository on BitBucket.
Anyway, the BitBuket service was excellent.

Follow me on: https://github.com/olivierberthomme/sqlsrvscripts